import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListPageRoutingModule } from './list-routing.module';

import { ListPage } from './list.page';
import { ItemPage } from '../item/item.page';
import { ToItemPipe } from '../pipes/to-item.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListPageRoutingModule,
  ],
  declarations: [ListPage, ItemPage, ToItemPipe],
  exports: [
    ItemPage, ToItemPipe
  ]
})
export class ListPageModule {}
