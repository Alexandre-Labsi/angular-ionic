import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { VideoService } from '../service/video.service';
import { Movie } from './movie';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
  providers: [VideoService]
})
export class ListPage implements OnInit, OnDestroy {

  private sub$: Subscription;

  protected movies: Movie[];
  constructor( private service: VideoService ) { }

  
  ngOnInit(): void {
    this.service.getPopularMovies();
    this.sub$ = this.service.getObservable()
    .subscribe((data: Movie[])=> this.movies = data)
  }


  ngOnDestroy(): void {
    this.sub$.unsubscribe();
  }
}
