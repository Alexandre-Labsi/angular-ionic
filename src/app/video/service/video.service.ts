import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Movie } from '../list/movie';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VideoService {
  
  private subject$: BehaviorSubject<Movie[]>;
  private key = "k_oqpsd1ex";
  private movies: Movie[];

  constructor( private http: HttpClient ) { 
    this.subject$ = new BehaviorSubject([]);
  }

  public getPopularMovies(): Subscription {
    return this.http.get<Movie[]>(`https://imdb-api.com/en/API/Top250Movies/${this.key}`)
    .pipe(map((data: any)=> data.items))
    .subscribe((data: Movie[]) => {
      this.subject$.next(data);
      this.movies = data;
    });
  }

  public getObservable(){
    return this.subject$.asObservable();
  }

  public getMovieByName(name: string){
    return this.http.get(`https://imdb-api.com/en/API/SearchMovie/${this.key}/${name}`)
    .pipe(map((data: any)=> data.results))
  }

  public deleteMovie(title: string){
      this.movies.map((m, i) => {
      if (m.title === title) {
        this.movies.splice(i, 1);
      }
      this.subject$.next(this.movies);
    });
  }

}
