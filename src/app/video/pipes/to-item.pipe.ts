import { Pipe, PipeTransform } from '@angular/core';
import { Movie } from '../list/movie';

@Pipe({
  name: 'toItem'
})
export class ToItemPipe implements PipeTransform {

  transform(val: Movie) {
    const item = {
      title: val.title,
      year: val.year,
      src: val.image
    }
    return item;
  }

}
