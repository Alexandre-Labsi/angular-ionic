import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from '../list/movie';
import { VideoService } from '../service/video.service';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  constructor(private service: VideoService, private route: ActivatedRoute) { }
  
  private title: string;

  protected movies : Movie;
  
  ngOnInit() {
    this.title = this.route.snapshot.paramMap.get('title');
    this.service.getMovieByName(this.title).subscribe(data => this.movies = data);

        
  }

}
